const { Fragment } = require("react");
const React = require("react");
const GlobalStyles = require("./src/theme/globalStyles").default;

exports.onRenderBody = ({ setHtmlAttributes, setHeadComponents }) => {
  setHtmlAttributes({
    lang: "en",
  });
  setHeadComponents([
    <script
      type="text/javascript"
      src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js"
      async
    />,
  ]);
};

exports.wrapPageElement = ({ element, props }) => {
  return (
    <Fragment>
      <GlobalStyles />
      {element}
    </Fragment>
  );
};
