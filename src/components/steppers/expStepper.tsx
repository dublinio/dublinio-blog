/** @jsx createElement */
import { createElement, useState } from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import StepContent from "@mui/material/StepContent";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";

const steps = [
  {
    label: "Head of engineering (July 2022 - Present)",
    description: `I work as head of engineering for Ding. My role comprises of leading a team of over 30 engineers across web, app and backend. 
    I work closely with product, QA as well as the other technology leads within the company to ensure best practices in all areas such as app features as well as security, monitoring and data. 
    I work to make sure a timely and effecient process between the teams exists as well as promoting the engineering department within the company. Part of my role is developing the tech stack and looking for tecnical improvements that will drive the team and company forward toward our shared goals.
    Hiring, onboarding and mentoring are important in my role.`,
  },
  {
    label: "Engineering team lead (Nov 2015 - July 2022)",
    description: `I work as a front-end developer and team lead for Ding.com. I joined as a Junior and have been promoted 3 times to now be a team lead.
    My role comprises of co-ordination of web developers across 3 teams. I work to make sure a timely and effecient process between the teams exists as well as promoting the web chapter within the company. Part of my role is developing the tech stack and looking for technical improvements that will drive the team and company forward toward our shared goals.
    Hiring, onboarding and mentoring are important in my role. I also still actively work as a front end developer here using the latest tech.`,
  },
  {
    label: "Project consultant (Jan 2020 - Present)",
    description:
      "I work with a number of companies in a consultancy role. This involves some development work along with infrastructural set-up. (moving from current hosting to AWS or a similar solution is a common ask) as well as liasing between non technical clients and developers to streamline the SDLC process.",
  },
  {
    label: "Other work",
    description: `I work with a couple of charities providing web design and general IT services. Aoibhneas and Sphenoid Ireland.`,
  },
];

export default function VerticalLinearStepper() {
  const [activeStep, setActiveStep] = useState(0);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <Box sx={{ maxWidth: 400 }}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((step, index) => (
          <Step key={step.label}>
            <StepLabel>{step.label}</StepLabel>
            <StepContent>
              <Typography>{step.description}</Typography>

              <Box sx={{ mb: 2 }}>
                <div>
                  <Button
                    variant="contained"
                    onClick={handleNext}
                    sx={{ mt: 1, mr: 1 }}
                  >
                    {index === steps.length - 1 ? "Finish" : "Continue"}
                  </Button>
                  <Button
                    variant="outlined"
                    disabled={index === 0}
                    onClick={handleBack}
                    sx={{ mt: 1, mr: 1 }}
                  >
                    Back
                  </Button>
                </div>
              </Box>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} sx={{ p: 3 }}>
          <Typography>You know all about me!</Typography>
          <Button onClick={handleReset} sx={{ mt: 1, mr: 1 }}>
            Reset
          </Button>
        </Paper>
      )}
    </Box>
  );
}
