/** @jsx createElement */
import { createElement, useEffect, useRef } from "react";

const TrustBox = () => {
  // Create a reference to the <div> element which will represent the TrustBox
  const ref = useRef(null);
  useEffect(() => {
    // If window.Trustpilot is available it means that we need to load the TrustBox from our ref.
    // If it's not, it means the script you pasted into <head /> isn't loaded  just yet.
    // When it is, it will automatically load the TrustBox.
    if ((window as any).Trustpilot) {
      (window as any).Trustpilot.loadFromElement(ref.current, true);
    }
  }, []);
  return (
    <div
      style={{ position: "absolute" }}
      data-locale="en-US"
      data-template-id="5419b6a8b0d04a076446a9ad"
      data-businessunit-id="6287ec02d1a7222a96f0ab26"
      data-style-height="24px"
      data-style-width="100%"
      data-theme="light"
      data-min-review-count="10"
      data-without-reviews-preferred-string-id="1"
      ref={ref} // We need a reference to this element to load the TrustBox in the effect.
      className="trustpilot-widget" // Renamed this to className.
      // [ long list of data attributes...]
    >
      <a
        href="https://www.trustpilot.com/review/michaelcj.com"
        target="_blank"
        rel="noopener"
      >
        Trustpilot
      </a>
    </div>
  );
};
export default TrustBox;
