import React from "react";
import { Helmet } from "react-helmet";
import home from "../../images/appdev.png";

export interface SiteMeta {
  title: string;
  description: string;
  canonical: string;
  index?: boolean;
}

interface Props {
  data: SiteMeta;
}

const HelmetSeo = (props: Props) => {
  const { data } = props;

  if (!data) {
    return null;
  }

  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{data.title}</title>
      <meta name="theme-color" content="#ffffff"></meta>
      <meta
        name="description"
        property="og:description"
        content={data.description}
      />
      <meta name="title" property="og:title" content={data.title} />
      <link rel="canonical" href={data.canonical} />
      {data.index && <meta name="robots" content="follow, index"></meta>}
      <meta property="og:title" content={data.title} />
      <meta property="og:image" content={`https://www.michaelcj.com${home}`} />
      <meta
        property="og:description"
        content="Michael James | Project consultant and senior software developer specializing in front end development and managment of software projects and their lifecycles"
      />
      <meta property="og:url" content="https://michaelcj.com/" />
      <meta property="og:type" content="website" />
      <meta property="og:image" content={`https://www.michaelcj.com${home}`} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta property="twitter:domain" content="michaelcj.com" />
      <meta property="twitter:url" content="https://michaelcj.com/" />
      <meta name="twitter:title" content="Michael James | Consultant" />
      <meta
        name="twitter:description"
        content="Michael James | Project consultant and senior software developer specializing in front end development and managment of software projects and their lifecycles"
      />
      <meta name="twitter:image" content={`https://www.michaelcj.com${home}`} />
    </Helmet>
  );
};

export default HelmetSeo;
