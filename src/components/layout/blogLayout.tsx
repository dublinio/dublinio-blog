import Header from "../navigation/header";
import styled from "styled-components";
import React from "react";
import Footer from "../navigation/footer";
import Zoom from "@mui/material/Zoom";
import Typography from "@mui/material/Typography";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";
import { useLocation } from "@reach/router";

const LayoutStyle = styled.div`
  margin-top: 70px;
`;

const BlogLayout = ({ pageTitle, children, isBlogPost = false }) => {
  const location = useLocation();

  return (
    <Wrap>
      <Header active="blogs" />
      <LayoutStyle>
        {pageTitle !== "" && (
          <Zoom in={true} style={{ transitionDelay: "100ms" }}>
            <h1>{pageTitle}</h1>
          </Zoom>
        )}
      </LayoutStyle>

      {children}

      {isBlogPost && (
        <Breadcrumbs aria-label="breadcrumb">
          <Link underline="hover" color="inherit" href="/">
            home
          </Link>
          <Link underline="hover" color="inherit" href="/blogs">
            blogs
          </Link>
          <Typography color="text.primary">
            {location.pathname.replace("/blogs/", "")}
          </Typography>
        </Breadcrumbs>
      )}
      <Footer />
    </Wrap>
  );
};

const Wrap = styled.div`
  margin: 0 auto;
  img {
    max-width: 350px;
  }
`;

export default BlogLayout;
