import Header from "../navigation/header";
import styled from "styled-components";
import React, { useEffect } from "react";
import Footer from "../navigation/footer";
import Zoom from "@mui/material/Zoom";

const LayoutStyle = styled.div`
  margin-top: 70px;
`;

const Layout = ({ pageTitle, children, page }) => {
  useEffect(() => window.scrollTo(0, 0), []);

  return (
    <Wrap>
      <Header active={page} />
      <LayoutStyle>
        {pageTitle !== "" && (
          <Zoom in={true} style={{ transitionDelay: "100ms" }}>
            <h1>{pageTitle}</h1>
          </Zoom>
        )}
      </LayoutStyle>

      {children}
      <Footer />
    </Wrap>
  );
};

const Wrap = styled.div`
  margin: 0 auto;
`;

export default Layout;
