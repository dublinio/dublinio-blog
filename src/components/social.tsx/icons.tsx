/** @jsx createElement */
import { createElement } from "react";
import styled from "styled-components";
import linkedin from "../../images/linkedin.svg";
import facebook from "../../images/facebook.svg";
import git from "../../images/git.svg";
import home from "../../images/homeicon.svg";
import email from "../../images/email.svg";
import Zoom from "@mui/material/Zoom";
import { fbLink, gitLink, linkedinLink } from "../../constants/siteUrls";

const Wrap = styled.div``;
interface IconProps {
  showHome?: boolean;
  showGithub?: boolean;
}

function Icons(props: IconProps) {
  const { showHome, showGithub } = props;

  return (
    <Wrap>
      {showHome && (
        <Zoom in={true} style={{ transitionDelay: "400ms" }}>
          <a aria-label="link to home page" href="/">
            <img src={home} alt="home" />
          </a>
        </Zoom>
      )}
      <Zoom in={true} style={{ transitionDelay: "500ms" }}>
        <a aria-label="link to linkedin" target="_blank" href={linkedinLink}>
          <img src={linkedin} alt="linkedin" />
        </a>
      </Zoom>
      <Zoom in={true} style={{ transitionDelay: "600ms" }}>
        <a aria-label="link to facebook" target="_blank" href={fbLink}>
          <img src={facebook} alt="facebook" />
        </a>
      </Zoom>
      <Zoom in={true} style={{ transitionDelay: "650ms" }}>
        <a
          aria-label="link to email"
          target="mailto:michaelcjames10@gmail.com"
          href="mailto:michaelcjames10@gmail.com"
        >
          <img src={email} alt="email" />
        </a>
      </Zoom>
      {showGithub && (
        <Zoom in={true} style={{ transitionDelay: "700ms" }}>
          <a aria-label="link to github" target="_blank" href={gitLink}>
            <img src={git} alt="git" />
          </a>
        </Zoom>
      )}
    </Wrap>
  );
}

export default Icons;
