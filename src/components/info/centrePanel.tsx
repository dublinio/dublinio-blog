/** @jsx createElement */
import { createElement, ReactNode } from "react";
import styled from "styled-components";
import { Button } from "@mui/material";
import { Link } from "gatsby";

const DetailGrid = styled.div`
  text-align: center;

  > * {
    margin-left: auto;
    margin-right: auto;
  }
`;

interface Props {
  text: string;
  title: string;
  image: ReactNode;
  link?: string;
  imageSize?: number;
}

function CenterPanel(p: Props) {
  const { text, title, image, link } = p;
  return (
    <DetailGrid>
      {image}
      <h3>{title}</h3>
      <p>{text}</p>
      {link && (
        <Link to={link}>
          <Button size="small" variant="outlined">
            read more
          </Button>
        </Link>
      )}
    </DetailGrid>
  );
}

export default CenterPanel;
