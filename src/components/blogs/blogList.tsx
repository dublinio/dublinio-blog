import React from "react";
import { Link, graphql, useStaticQuery } from "gatsby";
import styled from "styled-components";
import { formatDate } from "../../services/date/date";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import Zoom from "@mui/material/Zoom";
import { palette } from "../../theme/palette";

interface BlogItem {
  contentful_id: string;
  createdAt: string;
  shortDescription: string;
  slug: string;
  title: string;
  content: {
    raw: string;
  };
}

const LatestArcticles = styled.div`
  background: #fff;
  margin-bottom: 50px;

  a {
    color: ${palette.darkmain};
  }

  li {
    padding-left: 0px;
  }
`;

const ListItemWrap = styled(ListItem)`
  padding-left: 0px;
`;

const BlogList = () => {
  const data = useStaticQuery(graphql`
    query BlogIndexQuery {
      allContentfulAsset {
        edges {
          node {
            id
          }
        }
      }
      allContentfulBlogPost {
        nodes {
          contentful_id
          createdAt
          title
          slug
          content {
            raw
          }
          shortDescription
        }
      }
    }
  `);

  const toRender = data.allContentfulBlogPost.nodes;

  return (
    <Zoom in={true} style={{ transitionDelay: "400ms" }}>
      <LatestArcticles>
        <List
          sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        >
          {toRender.map((node: BlogItem, _i: number) => (
            <Link key={node.createdAt} to={`/blogs/${node.slug}`}>
              <ListItemWrap>
                <ListItemAvatar>
                  <Avatar sx={{ bgcolor: "#00ebc7" }} />
                </ListItemAvatar>
                <ListItemText
                  primary={node.title}
                  secondary={formatDate(node.createdAt)}
                />
              </ListItemWrap>
            </Link>
          ))}
        </List>
      </LatestArcticles>
    </Zoom>
  );
};

export default BlogList;
