import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";

import styled from "styled-components";
import { Skeleton } from "@mui/material";

const ImageSkeleton = styled(Skeleton)`
  max-width: 100%;
`;

const Wrap = styled.div`
  position: relative;
`;

export default function ImageLoader({ alt, src, width, height, lazy }) {
  const [loaded, setLoaded] = useState(false);
  const [hasError, setHasError] = useState(false);
  const imgRef = useRef<HTMLImageElement>(null);

  useEffect(() => {
    if (imgRef.current?.complete) {
      onLoaded();
    }
  }, []);

  const onLoaded = () => {
    setLoaded(true);
  };

  if (hasError) {
    return null;
  }
  return (
    <Wrap style={{ height: height, width: width, maxWidth: "100%" }}>
      {!loaded && (
        <ImageSkeleton
          sx={{ bgcolor: "rgb(241 241 241 / 11%)" }}
          animation="wave"
          variant="rectangular"
          width={width}
          height={height}
        />
      )}
      <img
        ref={imgRef}
        onLoad={onLoaded}
        loading={lazy ? "lazy" : "eager"}
        alt={alt}
        src={src}
        width={width}
        height={height}
        style={{
          height: height,
          width: width,
          position: "absolute",
          top: "0px",
          visibility: loaded ? "visible" : "hidden",
          left: "0px",
          right: "0px",
          maxWidth: "100%",
        }}
        onError={() => {
          console.log("ERROR");
          setHasError(true);
        }}
      />
    </Wrap>
  );
}

ImageLoader.propTypes = {
  alt: PropTypes.string.isRequired,
};

ImageLoader.defaultProps = {
  lazy: false,
};
