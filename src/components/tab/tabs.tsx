/** @jsx createElement */
import { createElement, SyntheticEvent, useState } from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export const BasicTabs = () => {
  const [value, setValue] = useState(0);

  const handleChange = (event: SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="Planning" {...a11yProps(0)} />
          <Tab label="Implementation" {...a11yProps(1)} />
          <Tab label="Post release" {...a11yProps(2)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        Once a project receives the green light, it needs a solid plan to guide
        the team, as well as keep them on time and on budget. A well-written
        project plan gives guidance for obtaining resources, acquiring financing
        and procuring required materials. The project plan gives the team
        direction for producing quality outputs, handling risk, creating
        acceptance, communicating benefits to stakeholders and managing
        suppliers. The project plan also prepares teams for the obstacles they
        might encounter over the course of the project, and helps them
        understand the cost, scope and timeframe of the project.
      </TabPanel>
      <TabPanel value={value} index={1}>
        This is the phase that is most commonly associated with project
        management. Execution is all about building deliverables that satisfy
        the customer. Team leaders make this happen by allocating resources and
        keeping team members focused on their assigned tasks. Execution relies
        heavily on the planning phase. The work and efforts of the team during
        the execution phase are derived from the project plan.
      </TabPanel>
      <TabPanel value={value} index={2}>
        Monitoring and control are sometimes combined with execution because
        they often occur at the same time. As teams execute their project plan,
        they must constantly monitor their own progress. To guarantee delivery
        of what was promised, teams must monitor tasks to prevent scope creep,
        calculate key performance indicators and track variations from allotted
        cost and time. This constant vigilance helps keep the project moving
        ahead smoothly.
      </TabPanel>
    </Box>
  );
};
