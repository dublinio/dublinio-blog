/** @jsx createElement */
import { createElement } from "react";
import Button from "@mui/material/Button";
import { navigate } from "gatsby";

const HireMeBtn = () => {
  const onClick = () => {
    navigate("/contact");
  };

  return (
    <Button onClick={onClick} variant="contained">
      CONTACT ME
    </Button>
  );
};
export default HireMeBtn;
