/** @jsx createElement */
import { createElement, Fragment, useState } from "react";
import styled, { keyframes } from "styled-components";
import closeicon from "../../images/close.svg";
import menuicon from "../../images/menu.svg";
import { palette } from "../../theme/palette";
import { Link, navigate } from "gatsby";
import { siteUrls } from "../../constants/siteUrls";
import Grow from "@mui/material/Grow";
import Typography from "@mui/material/Typography";
import Fade from "@mui/material/Fade";
import HomeIcon from "@mui/icons-material/Home";

const HeaderStyle = styled.header`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  justify-content: space-between;
  margin-top: 40px;
`;
const Home = styled.div`
  cursor: pointer;
  margin-right: 10px;
  margin-bottom: -6px;
`;
const LinkStyle = styled(Link)<{ active: boolean }>`
  cursor: pointer;
  margin-right: 16px;
  font-weight: bold;
  display: flex;
  align-items: center;
  justify-content: center;

  p {
    margin-top: 0px;
    margin-bottom: 0px;
  }
`;
const WidthGrow = keyframes`
  0% { width: 20%; }
  100% { width: 100%;}
`;

const ActiveLink = styled.div`
  position: absolute;
  bottom: -4px;
  width: 100%;
  background: #000;
  height: 3px;
  transition: all 0.15s;
  animation: ${WidthGrow};
  animation-duration: 0.15s;
  animation-fill-mode: forwards;
`;
const Links = styled.div<{ active: boolean }>`
  display: flex;
  visibility: hidden;

  a {
    color: #3f3d56;
    font-size: 16px;
    position: relative;
    text-transform: capitalize;
  }

  @media only screen and (min-width: ${palette.breakpoint}) {
    visibility: visible;
  }
`;

const MenuIcon = styled.img<{ isOpen: boolean }>`
  position: ${({ isOpen }) => (isOpen ? "fixed" : "absolute")};
  width: 50px;
  left: 20px;
  top: 24px;
  z-index: 3;

  @media only screen and (min-width: ${palette.breakpoint}) {
    visibility: hidden;
  }
`;

const Menu = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: block;
  text-align: center;
  padding-top: 20%;
  background: #fff;
  z-index: 2;

  @media only screen and (min-width: ${palette.breakpoint}) {
    pointer-events: none;
    visibility: hidden;
  }
`;

const MobileLink = styled.div<{ index: number }>`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 16px;

  a {
    color: #3f3d56;
    font-size: 20px;
    text-transform: lowercase;
    font-family: Roboto-Bold;
  }

  a {
    margin: 0;
  }
`;

interface Props {
  active: string;
}

function Header(props: Props) {
  const [open, setOpen] = useState<boolean>(false);
  const { active } = props;
  return (
    <Fragment>
      <HeaderStyle>
        <MenuIcon
          alt="menu icon"
          isOpen={open}
          src={!open ? menuicon : closeicon}
          onClick={() => {
            setOpen(!open);
          }}
        />
        <Links>
          <Home
            onClick={() => {
              navigate("/");
            }}
          >
            <HomeIcon fontSize="large" />
          </Home>
          {siteUrls.map((siteUrl) => {
            return (
              siteUrl.showOnDesktop && (
                <Fragment key={siteUrl.slug + "desktop"}>
                  {siteUrl.badge ? (
                    <LinkStyle to={siteUrl.slug}>
                      {siteUrl.display}
                      {active === siteUrl.slugClean && <ActiveLink />}
                    </LinkStyle>
                  ) : (
                    <LinkStyle
                      key={siteUrl.slug + "link_desktop"}
                      to={siteUrl.slug}
                    >
                      {siteUrl.display}
                      {active === siteUrl.slugClean && (
                        <Fade in>
                          <ActiveLink />
                        </Fade>
                      )}
                    </LinkStyle>
                  )}
                </Fragment>
              )
            );
          })}
        </Links>
      </HeaderStyle>

      {open && (
        <Grow in={true} style={{ transitionDelay: "100ms" }}>
          <Menu>
            {siteUrls.map((siteUrl, index) => {
              return (
                <MobileLink
                  data-active={active}
                  key={siteUrl.slug + "mobile"}
                  index={index + 0.5}
                >
                  {siteUrl.badge ? (
                    <LinkStyle
                      data-active={active === siteUrl.slugClean}
                      key={index}
                      to={siteUrl.slug}
                    >
                      {siteUrl.display}
                    </LinkStyle>
                  ) : (
                    <LinkStyle
                      data-active={active === siteUrl.slugClean}
                      key={siteUrl.slug + "link_mobile"}
                      to={siteUrl.slug}
                    >
                      {siteUrl.display}
                    </LinkStyle>
                  )}
                </MobileLink>
              );
            })}
            <Typography variant="caption" display="block" gutterBottom>
              start growing your business today!
            </Typography>
          </Menu>
        </Grow>
      )}
    </Fragment>
  );
}

export default Header;
