/** @jsx createElement */
import { createElement, useEffect, useState } from "react";
import styled from "styled-components";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import linkedin from "../../images/linkedin.svg";
import facebook from "../../images/facebook.svg";
import email from "../../images/email.svg";
import Grid from "@mui/material/Grid";
import { palette } from "../../theme/palette";
import { linkedinLink, fbLink } from "../../constants/siteUrls";
import Link from "@mui/material/Link";
import { getYear } from "../../services/date/date";
import TrustBox from "../widgets/trustpilot";

const Wrap = styled.div`
  margin-top: 200px;
  padding-bottom: 100px;
`;
const FooterStyle = styled.div`
  padding-top: 20px;

  a {
    text-align: left;
    justify-content: flex-start;
    color: gray;
    font-size: 14px;
    text-decoration: none;
  }
`;

const FooterMuted = styled.p`
  color: gray;
  font-size: 14px;
  margin-bottom: 0px;
`;

const Social = styled.img`
  width: 25px;
  opacity: 0.7;
`;

const SocialRow = styled(Stack)`
  justify-content: end;

  @media only screen and (max-width: ${palette.tabletbreakpoint}) {
    justify-content: start;
  }
`;
const DetailRow = styled(Stack)`
  justify-content: start;
`;

const FooterLink = styled(Link)`
  padding: 6px 0px;
`;

function Footer() {
  const [hasMounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  return (
    <Wrap>
      <Divider />
      <FooterStyle>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <DetailRow spacing={1} direction="column">
              <FooterLink href="/web-design">Web design</FooterLink>
              <FooterLink href="/project-management">
                Project management
              </FooterLink>
              <FooterLink href="/blogs">Blogs</FooterLink>
              <FooterLink href="/about-me">About me</FooterLink>
              <FooterLink href="/contact">Contact</FooterLink>
              <FooterLink href="/faq">FAQ</FooterLink>
              <FooterLink href="/policy">Policy</FooterLink>
            </DetailRow>
            <FooterMuted>Copyright @Michael James {getYear()}</FooterMuted>
          </Grid>
          <Grid item xs={12} md={6}>
            <SocialRow spacing={1} direction="row">
              <a href={linkedinLink}>
                <Social src={linkedin} alt="linkedin" />
              </a>
              <a href={fbLink}>
                <Social src={facebook} alt="facebook" />
              </a>
              <a href="mailto:michaelcjconsultancy@gmail.com">
                <Social src={email} alt="email" />
              </a>
            </SocialRow>
          </Grid>
        </Grid>
        {hasMounted && !!window && (
          <div>
            <TrustBox />
          </div>
        )}
      </FooterStyle>
    </Wrap>
  );
}

export default Footer;
