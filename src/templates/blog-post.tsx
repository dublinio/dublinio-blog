import React from "react";
import { graphql } from "gatsby";
import BlogLayout from "../components/layout/blogLayout";
import { BLOCKS } from "@contentful/rich-text-types";
import { renderRichText } from "gatsby-source-contentful/rich-text";
import HelmetSeo, { SiteMeta } from "../components/seo/helmet";
import Zoom from "@mui/material/Zoom";

const Text = ({ children }) => <p className="align-center">{children}</p>;
const options = {
  renderNode: {
    [BLOCKS.PARAGRAPH]: (_node, children) => <Text>{children}</Text>,
    [BLOCKS.EMBEDDED_ASSET]: (node) => {
      console.log(node);
      return (
        <img
          src={node.data.target.file.url}
          width="100%"
          height="337px"
          alt=""
        />
      );
    },
  },
};

const BlogPostTemplate = ({ data }) => {
  const meta: SiteMeta = {
    canonical: data.contentfulBlogPost.metaInfo.canonical,
    title: data.contentfulBlogPost.metaInfo.metaTitle,
    description: data.contentfulBlogPost.metaInfo.metaDescription,
  };

  return (
    <BlogLayout isBlogPost pageTitle="">
      <HelmetSeo data={meta} />
      <Zoom in={true} style={{ transitionDelay: "100ms" }}>
        <h1>{data.contentfulBlogPost.title}</h1>
      </Zoom>
      {renderRichText(data.contentfulBlogPost.content, options)}
    </BlogLayout>
  );
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    contentfulBlogPost(slug: { eq: $slug }) {
      slug
      title
      metaInfo {
        metaTitle
        metaDescription
        canonical
      }
      content {
        raw
        references {
          ... on ContentfulAsset {
            __typename
            contentful_id
            file {
              url
            }
          }
        }
      }
    }
  }
`;
