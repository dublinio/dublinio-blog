/** @jsx createElement */
import { createElement, useEffect } from "react";
import styled from "styled-components";
import Layout from "../../components/layout/layout";
import BlogList from "../../components/blogs/blogList";
import HelmetSeo from "../../components/seo/helmet";

const BlogStyle = styled.div`
  min-height: 700px;
`;

const BlogPage = () => {
  useEffect(() => window.scrollTo(0, 0), []);
  return (
    <Layout pageTitle="Latest blogs" page="blogs">
      <HelmetSeo
        data={{
          canonical: "",
          description: "Michael James is a software development consultant",
          title: "Michael James | Blogs",
          index: true,
        }}
      />
      <BlogStyle>
        <BlogList />
      </BlogStyle>
    </Layout>
  );
};

export default BlogPage;
