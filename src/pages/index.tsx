/** @jsx createElement */
import { createElement } from "react";
import styled from "styled-components";
import { palette } from "../theme/palette";
import home from "../images/react.svg";
import rocket from "../images/rocket.svg";
import HelmetSeo from "../components/seo/helmet";
import Layout from "../components/layout/layout";
import Grid from "@mui/material/Grid";
import Zoom from "@mui/material/Zoom";
import ImageLoader from "../components/images/imageLoader";
import HireMeBtn from "../components/navigation/hiremeBtn";
import CenterPanel from "../components/info/centrePanel";
import BlogList from "../components/blogs/blogList";
import WebIcon from "@mui/icons-material/Web";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import DesignServicesIcon from "@mui/icons-material/DesignServices";

const LandingStyle = styled.div`
  text-align: center;
  margin-top: 70px;
  min-height: 400px;

  p {
    max-width: 400px;
  }

  @media only screen and (max-width: ${palette.breakpoint}) {
    h3 {
      max-width: 250px;
    }
  }
`;

const HeroDetail = styled.div`
  text-align: left;
`;

const ServicesContainer = styled.div`
  margin-top: 120px;
  @media only screen and (max-width: ${palette.breakpoint}) {
    margin-top: 60px;
  }
`;

const AboutBlock = styled.div`
  text-align: center;
  @media only screen and (max-width: ${palette.breakpoint}) {
  }
`;

function Home() {
  return (
    <Layout pageTitle="" page="">
      <LandingStyle>
        <HelmetSeo
          data={{
            canonical: "",
            description: "Michael James is a software development consultant",
            title: "Michael James | Consultant",
            index: true,
          }}
        />

        <Grid container spacing={2}>
          <Grid item xs={12} md={5}>
            <HeroDetail>
              <Zoom in={true} style={{ transitionDelay: "100ms" }}>
                <h1>Hi, I'm Michael.</h1>
              </Zoom>
              <h3>Lead Software engineer. Project consultant.</h3>
              <p>
                Experienced software developer specialising in website
                development, UX and project management and leading engineering
                teams
              </p>

              <HireMeBtn />
            </HeroDetail>
          </Grid>
          <Grid item xs={12} md={7}>
            <ImageLoader
              alt=""
              width={600}
              height={400}
              lazy={false}
              src={home}
            />
          </Grid>
        </Grid>
      </LandingStyle>
      <ServicesContainer>
        <Grid container spacing={4}>
          <Grid item xs={12} md={4}>
            <CenterPanel
              text="I make custom, dynamic software for a range of customers from a small or personal business to larger enterprises. "
              title="Web development"
              image={<WebIcon fontSize="large" />}
              link="web-design"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <CenterPanel
              text="Making sure both parties, development and the client have a stress free and enjoyable project experience"
              title="Project management"
              image={<ManageAccountsIcon fontSize="large" />}
              link="project-management"
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <CenterPanel
              text="Fully customized and branded designs to your specifications meeting all accessibility best practices"
              title="UX design"
              image={<DesignServicesIcon fontSize="large" />}
              link="web-design"
            />
          </Grid>
        </Grid>
      </ServicesContainer>

      <ServicesContainer>
        <Grid container spacing={4}>
          <Grid item xs={12} md={6}>
            <AboutBlock>
              <ImageLoader
                alt=""
                width={500}
                height={350}
                lazy={false}
                src={rocket}
              />
            </AboutBlock>
          </Grid>
          <Grid item xs={12} md={6}>
            <AboutBlock>
              <h2>Why work with me?</h2>
              <p>
                I am a highly motivated and experienced and a great
                communicator. Not everyone is tecnical and I take every clients
                different level of experience in IT before and during all
                projects. Attention to detail is very important to me as well as
                getting things done on time.
              </p>
            </AboutBlock>
          </Grid>
        </Grid>
      </ServicesContainer>
      <ServicesContainer>
        <h2>Latest blogs</h2>
        <Grid container spacing={4}>
          <Grid item xs={12} md={6}>
            <BlogList />
          </Grid>
        </Grid>
      </ServicesContainer>
    </Layout>
  );
}

export default Home;
