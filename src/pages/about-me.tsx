/** @jsx createElement */
import { createElement } from "react";
import Layout from "../components/layout/layout";
import HelmetSeo from "../components/seo/helmet";
import Grid from "@mui/material/Grid";
import home from "../images/tech.svg";
import ImageLoader from "../components/images/imageLoader";
import VerticalLinearStepper from "../components/steppers/expStepper";
import HireMeBtn from "../components/navigation/hiremeBtn";

const ExpPage = () => {
  return (
    <Layout pageTitle="About me" page="about-me">
      <HelmetSeo
        data={{
          canonical: "",
          description: "Everything you need to know about Michael",
          title: "Michael James | About me",
          index: true,
        }}
      />

      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <h3>What is my experience?</h3>
          <VerticalLinearStepper />
        </Grid>
        <Grid item xs={12} md={6}>
          <h3>What technologies do I work with?</h3>
          <ImageLoader
            alt=""
            width={500}
            height={300}
            lazy={false}
            src={home}
          />
          <p>
            I currently work with Gatsby, React, Redux, Styled Components,
            Storybook as well as some backend techs such as Node Js and C#. I
            also have .NET experience and a good experience with Java. On a
            front end UX/UI perspective I work with photoshop as well as Zeplin
            and UXpin.
          </p>
          <h3>Software lifecycles / SEO / Performance </h3>
          <p>
            I use Kanban and I have good experience with this as well as Agile.
            I have vast experience with SEO, analytics and web performance and
            have performance at the forefront of my thoughts in particular. My
            role requires a data driven approach to our processes and I
            particularly enjoy digging through data to see where the site can be
            improved. I have gained good testing experience here working
            regularly on Auto/System and Unit tests. I have also been involved
            with integrating third party solutions such as Facebook/Google
            login, Trust pilot and One Signal among others.
          </p>
          <HireMeBtn />
        </Grid>
      </Grid>
    </Layout>
  );
};
export default ExpPage;
