/** @jsx createElement */
import { createElement } from "react";
import Layout from "../components/layout/layout";
import HelmetSeo from "../components/seo/helmet";
import Icons from "../components/social.tsx/icons";

const ContactPage = () => {
  return (
    <Layout pageTitle="Contact me" page="contact">
      <HelmetSeo
        data={{
          canonical: "",
          description: "Contact now to get online",
          title: "Michael James | Contact",
          index: true,
        }}
      />
      <h3>Want to chat? Use one of these</h3>
      <Icons />
    </Layout>
  );
};
export default ContactPage;
