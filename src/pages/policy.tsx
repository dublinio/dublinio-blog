/** @jsx createElement */
import { createElement } from "react";
import Layout from "../components/layout/layout";
import HelmetSeo from "../components/seo/helmet";
import Grid from "@mui/material/Grid";

const Policy = () => {
  return (
    <Layout pageTitle="Policy" page="policy">
      <HelmetSeo
        data={{
          canonical: "",
          description: "My company policy",
          title: "Michael James | Policy",
          index: true,
        }}
      />
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <h3>Common</h3>
          Common In common with other websites, log files are stored on the web
          server saving details such as the visitor's IP address, browser type,
          referring page and time of visit. Cookies may be used to remember
          visitor preferences when interacting with the website. Where
          registration is required, the visitor's email and a username will be
          stored on the server.
          <h3>Stored infomation</h3>
          The information is used to enhance the vistor's experience when using
          the website to display personalised content and possibly advertising.
          E-mail addresses will not be sold, rented or leased to 3rd parties.
          E-mail may be sent to inform you of news of our services or offers by
          us or our affiliates.
          <h3>Cookies</h3>
          Cookies are small digital signature files that are stored by your web
          browser that allow your preferences to be recorded when visiting the
          website. Also they may be used to track your return visits to the
          website. 3rd party advertising companies may also use cookies for
          tracking purposes.
        </Grid>
      </Grid>
    </Layout>
  );
};
export default Policy;
