/** @jsx createElement */
import { createElement, Fragment } from "react";
import Layout from "../components/layout/layout";
import HelmetSeo from "../components/seo/helmet";
import Grid from "@mui/material/Grid";
import { graphql } from "gatsby";
import { Helmet } from "react-helmet";

export const query = graphql`
  query FAQPageQuery {
    allContentfulFaqItem {
      edges {
        node {
          id
          text
          title
        }
      }
    }
  }
`;

interface FaqItem {
  node: {
    createdAt: string;
    text: string;
    title: string;
  };
}

const FaqPage = ({ data }) => {
  const items: FaqItem[] = data.allContentfulFaqItem.edges;

  const getMainEntities = () => {
    let all: any = [];

    items.forEach((item) => {
      all.push({
        "@type": "Question",
        name: item.node.title,
        acceptedAnswer: {
          "@type": "Answer",
          text: item.node.text,
        },
      });
    });

    return all;
  };

  const ldJson = {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    mainEntity: getMainEntities(),
  };

  return (
    <Layout pageTitle="FAQ" page="faq">
      <HelmetSeo
        data={{
          canonical: "",
          description: "Frequent questions asked by our users",
          title: "Michael James | Faq",
          index: true,
        }}
      />
      <Helmet>
        <script type="application/ld+json">{JSON.stringify(ldJson)}</script>
      </Helmet>
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          {items.map((item, i) => {
            return (
              <Fragment key={i}>
                <h3>{item.node.title}</h3>
                <p>{item.node.text}</p>
              </Fragment>
            );
          })}
        </Grid>
      </Grid>
    </Layout>
  );
};
export default FaqPage;
