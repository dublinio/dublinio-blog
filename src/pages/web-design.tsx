/** @jsx createElement */
import { createElement } from "react";
import Layout from "../components/layout/layout";
import HelmetSeo from "../components/seo/helmet";
import home from "../images/website.svg";
import ImageLoader from "../components/images/imageLoader";
import HireMeBtn from "../components/navigation/hiremeBtn";
import Grid from "@mui/material/Grid";
import styled from "styled-components";
import { palette } from "../theme/palette";
import CenterPanel from "../components/info/centrePanel";
import DesignServicesIcon from "@mui/icons-material/DesignServices";
import SavedSearchIcon from "@mui/icons-material/SavedSearch";
import MobileFriendlyIcon from "@mui/icons-material/MobileFriendly";

const ServicesContainer = styled.div`
  margin-top: 80px;
  @media only screen and (max-width: ${palette.breakpoint}) {
    margin-top: 40px;
  }
`;

const WebPage = () => {
  return (
    <Layout pageTitle="Web design" page="web-design">
      <HelmetSeo
        data={{
          canonical: "",
          description: "Web design services to put your business on the map",
          title: "Michael James | Web design",
          index: true,
        }}
      />

      <ServicesContainer>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <p>
              I make custom, dynamic software for a range of customers from a
              small or personal business to larger enterprises. I aim for a
              personal and communicative experience where customer satisfaction
              is key. All of my sites are SEO compliant as well as lightning
              fast. I aim to make getting your business or personal site online
              a painless and fast process. The sooner you're business is online
              the faster it will grow.
            </p>
            <HireMeBtn />
          </Grid>
          <Grid item xs={12} md={6}>
            <ImageLoader
              alt=""
              width={500}
              height={300}
              lazy={false}
              src={home}
            />
          </Grid>
        </Grid>
      </ServicesContainer>
      <ServicesContainer>
        <Grid container spacing={4}>
          <Grid item xs={12} md={4}>
            <CenterPanel
              text="All sites are fully mobile responsive on all devices. 80% of web traffic is mobile your site should support this"
              title="Responsive"
              image={<MobileFriendlyIcon fontSize="large" />}
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <CenterPanel
              text="Making sure both parties, development and the client have a stress free and enjoyable project experience"
              title="SEO compliant"
              image={<SavedSearchIcon fontSize="large" />}
            />
          </Grid>
          <Grid item xs={12} md={4}>
            <CenterPanel
              text="Fully customized and branded designs to your specifications meeting all accessibility best practices"
              title="Custom design"
              image={<DesignServicesIcon fontSize="large" />}
            />
          </Grid>
        </Grid>
      </ServicesContainer>
    </Layout>
  );
};
export default WebPage;
