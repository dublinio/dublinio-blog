/** @jsx createElement */
import { createElement } from "react";
import Layout from "../components/layout/layout";
import HelmetSeo from "../components/seo/helmet";
import Grid from "@mui/material/Grid";
import home from "../images/project.svg";
import ImageLoader from "../components/images/imageLoader";
import styled from "styled-components";
import { palette } from "../theme/palette";
import { BasicTabs } from "../components/tab/tabs";

const ServicesContainer = styled.div`
  margin-top: 80px;
  @media only screen and (max-width: ${palette.breakpoint}) {
    margin-top: 40px;
  }
`;

const Tabs = styled.div`
  margin-top: 50px;
`;

const ProjectPage = () => {
  return (
    <Layout pageTitle="Project management" page="project-management">
      <HelmetSeo
        data={{
          canonical: "",
          description:
            "Project management services to smooth your software devlivery phases",
          title: "Michael James | Project managment",
          index: true,
        }}
      />
      <ServicesContainer>
        <Grid container spacing={2}>
          <Grid item xs={12} md={8}>
            <p>
              I provide the service to manage a project lifecyle from
              requirements gathering all the way to final implementation. This
              includes security audits as well as performance reviews. I can be
              a point of contact between the client and developers to make sure
              non tecnical voices are heard.
            </p>
            <Tabs>
              <h3>Project phases</h3>

              <BasicTabs />
            </Tabs>
          </Grid>
          <Grid item xs={12} md={4}>
            <ImageLoader
              alt=""
              width={500}
              height={300}
              lazy={false}
              src={home}
            />
          </Grid>
        </Grid>
      </ServicesContainer>
    </Layout>
  );
};
export default ProjectPage;
