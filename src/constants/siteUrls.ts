export interface SiteUrl {
  slug: string;
  display: string;
  badge?: string;
  slugClean: string;
  showOnDesktop: boolean;
}

export const siteUrls: SiteUrl[] = [
  {
    display: "Home",
    slug: "/",
    slugClean: "",
    showOnDesktop: false,
  },
  {
    display: "Web design",
    slug: "/web-design",
    slugClean: "web-design",
    showOnDesktop: true,
  },
  {
    display: "Project management",
    slug: "/project-management",
    slugClean: "project-management",
    showOnDesktop: true,
  },
  {
    display: "About me",
    slug: "/about-me",
    slugClean: "about-me",
    showOnDesktop: true,
  },
  {
    display: "Blogs",
    slug: "/blogs",
    slugClean: "blogs",
    showOnDesktop: true,
  },
  {
    display: "Contact",
    slug: "/contact",
    slugClean: "contact",
    showOnDesktop: true,
  },
];

export enum SitePaths {
  Home = "/",
  Blogs = "/blogs",
  About = "/about",
  Policy = "/policy",
}

export const gitLink = "https://github.com/Michaelcj10/";
export const linkedinLink =
  "https://www.linkedin.com/in/michael-james-12b49968/";
export const fbLink = "https://www.facebook.com/michael.james.90475/";
